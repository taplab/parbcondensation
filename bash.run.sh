#!/bin/bash

#interaction strength
eps=5
#loading and recruitment rate
kappa=0.005


ncpu=2

mf=`pwd`
ff=$mf/MASTERFILES

for i in {1..1}
do 

mkdir REP${i}
cd REP${i}

cp ${ff}/run.lam .
cp ${ff}/Eq.patchpoly.n1.m1000.data .
cp ${ff}/parB_patches* .
cp ${ff}/bash.RNG.sh .
chmod a+x bash.RNG.sh

c++ parB_patches_v4.c++ -o parB_patches_v4 -O3
chmod a+x parB_patches_v4

r1=$(od -An -N2 -i /dev/urandom)
r2=$(od -An -N2 -i /dev/urandom)
r3=$(od -An -N2 -i /dev/urandom)
r4=$(od -An -N2 -i /dev/urandom)
r5=$(od -An -N2 -i /dev/urandom)
r1=$(( ${r1} + 1 ))
r2=$(( ${r2} + 1 ))
r3=$(( ${r3} + 1 ))
r4=$(( ${r4} + 1 ))
r5=$(( ${r5} + 1 ))

## STRENGTH OF INTERACTION
echo "variable eps equal ${eps}" >> parameters.dat

echo "variable seed equal ${r1}" > parameters.dat
echo "variable seed00 equal ${r2}" >> parameters.dat
echo "variable seedin equal ${r3}" >> parameters.dat
echo "variable seedtopo equal ${r4}" >> parameters.dat
echo "variable seedthermo equal ${r5}" >> parameters.dat
#echo "variable startfrom equal ${i}" >> parameters.dat

#see parB.c++ for mapping to real units
# N is polymer length
echo "variable N equal 1000" >> parameters.dat
#Diffusion coeff D = 0.05 um^2/s
#1 Brownian time is 0.5 us
#Sped up 100x D = 5 um^2/s = 0.062 \sigma^2/tau_B
# MSD = 2Dt so jump prob = 2*D = 0.124
#calling the jump ev 1 Brownian time
echo "variable Diffrate equal 0.124" >> parameters.dat

#Recruitment depends on CTP cycle = 50sec
#sped up 100x ~ 0.5s ~ 1e6 tau_b = 1e6 loops
#echo "variable recruitment equal 0.000001" >> parameters.dat
#sped up 100'000x ~ 0.5ms ~ 1e3 tau_b = 1e3 loops
echo "variable recruitment equal ${kappa}" >> parameters.dat
#concentration dependent - make it so 1e3 parB in full sim 1e6 loops -> 1/1e3
echo "variable loadingrate equal ${kappa}" >> parameters.dat

#Unloading depends on CTP cycle = 50s
#speed up 100x ~ 0.5s ~ 1/1e6
#echo "variable unloadingrate equal 0.000001" >> parameters.dat
#sped up 100'000x ~ 0.5ms ~ 1/1e3
echo "variable unloadingrate equal 0.001" >> parameters.dat

#THESE numbers below because in experiments it p_trans=11 p_cis and p_trans+p_cis=1
echo "variable cisrec equal 0.083" >> parameters.dat
echo "variable transrec equal 0.9166" >> parameters.dat

nohup ~/lmp_mpi -in run.lam < /dev/null > out &

cd ../

vi=$(( ${i}%${ncpu} ))

if(( ${vi} == 0 )); then
wait 
fi

done

wait

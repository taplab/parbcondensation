# parBcondensation

The file bash.run.sh is a bash script to run the simulations. 
The main parameters in this project to vary are strength of interaction and loading rate of parB.
The main LAMMPS commands file is run.lam in MASTERFILES. 
The main c++ code called to model parB is parB_patches_v4.c++ also in MASTERFILES. 
The INIT folder contains codes to create initial polymer conformation.


**Simulations of ParB on DNA**

We modelled the DNA as a semiflexible polymer made of 1000 spherical beads of size σ=5.5 nm=16bp. We placed parS in the middle of the chain where ParB beads are recruited and diffuse. The beads interact purely by excluded volume following the shifted and truncated Lennard-Jones (LJ) force field
	
	U_LJ (r,σ)= (4ε[(σ/r)^12-(σ/r)^6+1/4]   for r≤r_c and 0 otherwise 

where r denotes the distance between any two beads and r_c=2^(1/6) σ is the cut-off. We defined the bonds between two monomers along the DNA contour length by the finite extensible nonlinear elastic (FENE) potential, given by:

	U_FENE (r)=-0.5kR_o^2  log⁡(1-(r/R_o )^2 )   for r≤R_o

with k=30ε/σ^2 the spring constant and R_o=1.5σ the maximum length of the bond. We introduced the persistence length of the DNA chain as a bending potential energy between three consecutive beads given by:

	U_bend (θ)=k_θ (1-cosθ)

where θ is the angle between two bonds and k_θ=10 k_B T is the bending stiffness constant, corresponding to a persistence length of about 10 beads or ~55 nm. 

We performed the simulations in a constant-volume and constant-temperature (NVT) ensemble with implicit solvent utilizing the Langevin heat bath with local damping constant set to γ=1 so that the inertial time equals the Lennard- Jones time τ_MD=σ√(m/ε)=1 and m=1 is the mass of each bead. We let equilibrate the chain for long enough to reach a constant radius of gyration, before turning on the ParB recruitment and diffusion. We evolved the Langevin equation in LAMMPS (40) using a Velocity-Verlet integration scheme and a timestep dt=0.002. 
We performed mapping to real time and length units by using the Brownian time τ_B=500 dt=  γσ/(k_B T)=(3πη_S σ^3)/(k_B T). Using the viscosity of water η_S=1cP we obtained that τ_B=0.5 μs. We typically dump the positions of the beads every 10^4 τ_B= 5 ms. It’s also useful for later to note that with the coarse graining of 1 bead to 5.5 nm we also have that 1 μm= 182 σ. 
We modelled ParB in the system by calling, within the LAMMPS engine, an external program that modifies the types of the beads to account for the loading and diffusion of parB on DNA; in other words, parB is implicitly modelled. At t=0 we loaded a ParB protein onto the parS site and allowed it to diffuse with a constant D=5 (μm^2)/s=5 ((182^2 σ^2)/(2.8〖 10〗^6 τ_B ))=0.062 σ^2/τ_B .  Note that this is 100x faster than the real diffusion rate of parB, around 0.05 (μm^2)/s. The diffusion is implemented by attempting the movement of a loaded ParB either to its left or to its right with probability 0.125 every τ_B=500 dt timesteps (recall that MSD = 2 Dt in a 1D system, hence why the jump probability is twice the diffusion coefficient D). The diffusion cannot happen (the move is rejected) if the attempt brings a ParB protein either on top of another ParB or beyond the ends of the DNA.

On top of diffusion, we add a loading rate at ParS at a variable rate κ_on  and a unloading process at fixed rate  κ_off=0.001 〖1/τ〗_B, i.e. at a timescale T_off=0.5 ms. This timescale is faster than the one seen experimentally but we argue that the overall behaviour of the system is controlled by the ratio of loading and unloading timescales. In line with our previous work on the recruitment mechanism of parB proteins, we implement a stochastic recruitment at the same rate of the loading at ParS (κ_on), with the difference that the recruitment probability is associated with each loaded parB and can happen in-cis (with probability p_c=0.083) or in-trans (with probability p_T=1-p_C=0.9166), in such a way that the trans recruitment is 11 times larger than the cis recruitment. If the cis-recruitment is selected, one of the two 1D adjacent beads to a ParB is selected at random and, if unoccupied, a new ParB is added onto the chain. Otherwise, if the latter ‘in-trans’ mechanism is selected, we compute the list of 3D proximal neighbors which must be (i) within an Euclidean cutoff distance of 2σ=11 nm and (ii) farther than the second-nearest neighbour in 1D (i.e. the first and second nearest neighbors cannot be picked). Once the list of 3D neighbors is compiled, we randomly pick one of these from the list (if not empty), load a new ParB protein and resume the Langevin simulation.

**Implementation of Dimer-of-Dimers and Multimer-of-Dimers bridging**

To precisely regulate the binding mechanism between parB and control the valence of interactions we decorate each polymer bead with 2 patches at antipodal positions of the spherical bead. The central bead and the patches rotate as a rigid body and there is no constraint on the rotation of the patches, e.g. no dihedral potential is imposed. The interaction between patches is turned on only if the polymer bead is occupied by a ParB protein and we decide whether to turn one or both on. The former situation leads to the formation of one-to-one bridges (dimer-of-dimers) while the latter to the formation of multimers-of-dimers. The interaction between patches is regulated by a Morse potential of the form 

	U_Morse (r)=D_0  (Exp[-2α r]-2 Exp[-α r] ) , 

for r<r_c=2 σ and 0 otherwise. 
D_0  is a parameter we vary in the simulations to explore the phase diagram, while α=0.5 to ensure short-range interactions. We note that the cut off  r_c=2 σ=11nm is set on purpose to ensure that it is realistic for a pair-wise parB interaction.


/*
parB loads at parS (16bp=4 beads in the middle) and diffuses
parameters are
bead size sigma = 5.5 nm ~ 6 nm parB (6 nm) ~ parS site 16bp
1 tau_B = 100 steps = 0.5 us (for a bead size of 6 nm = 16 bp = parS)
1um ~ 3kbp = 183 beads (16 bp = 1 bead)
*****************************
DIFFUSION COEFFICIENT
****************************
D = 5 um^2/s = 0.062 sigma^2/tau_B
=> call parB ev Brownian time and move w prob 0.14
*/

#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include<unistd.h>
#include<functional>
#include<numeric>
#include<cstdlib>
#include <algorithm>
#include <random>
using namespace std;

double rate;
int mass[10];
int Ntot,ntype;
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
int timet;
double Lx,Ly,Lz;
string dummy;
int nangles,nangletypes,nbonds,nbondtypes;
	//
int id,mol,type;
double x,y,z;
int ix,iy,iz;
double vx,vy,vz;
	//
const int N=1;
const int Nbeads=1000;
const int npatches=3;

double Polymer[N][Nbeads][6]; //x,y,z,mol,type,id
double patch1[N][Nbeads][6]; //x,y,z,mol,type,id
double patch2[N][Nbeads][6]; //x,y,z,mol,type,id
double patch3[N][Nbeads][6]; //x,y,z,mol,type,id

double PolymerWrap[N][Nbeads][3]; //x,y,z
int PolymerImg[N][Nbeads][3]; //ix,iy,iz
int p1Img[N][Nbeads][3]; //ix,iy,iz
int p2Img[N][Nbeads][3]; //ix,iy,iz
int p3Img[N][Nbeads][3]; //ix,iy,iz
double Velocity[N][Nbeads*(npatches+1)][3];
int Angles[Nbeads][4]; //bead1,bead2,bead3,atype
int Bonds[Nbeads][3]; //bead1,bead2,btype
					  //
int nparb;
int parB[Nbeads]; //1D position of parB proteins
				  //
				  // PARAMETERS
double loadingrate, unloadingrate;
double diffrate;
double recruitment, cisrec, transrec;

double distance(double v1[6], double v2[6]);
int main(int argc, char* argv[]){
	cout << " ############################# EXTERNAL parB ################### " <<endl;
	int seedr=atoi(argv[10]);
	srand(seedr);
	double pp=rand();
	
	diffrate=atof(argv[4]);
	recruitment=atof(argv[5]);
	cisrec=atof(argv[6]);
	transrec=atof(argv[7]);
	loadingrate=atof(argv[8]);
	unloadingrate=atof(argv[9]);
	
	int time=atoi(argv[1]);
		//ARGV[1]=timestep
		//ARGV[2]=infile-no time
		//ARGV[3]=outfile-no time
	
		//to generate Gaussian numbers
	std::default_random_engine generator_parS;
	std::normal_distribution<double> distribution_parS(Nbeads/2.,2.0);
	
		//number of parBs
	nparb=0;
	
		//WRITE parB position
	stringstream writeFileLEF;
	writeFileLEF <<"parBlocation_"<<argv[3]<<"dat";
	ofstream writeLEF;
	writeLEF.open(writeFileLEF.str().c_str(), std::ios_base::app);
		//READ FILE
	ifstream indata;
	stringstream readFile;
	readFile.str("");
	readFile.clear();
	readFile << argv[2] << argv[1];
	indata.open(readFile.str().c_str());
	cout << readFile.str().c_str()<<endl;
	if(!indata.good()){cout << "AHAHAHAHHA"<<endl; cin.get();cin.get();cin.get();}
	
		//read 10 lines
	for(int i=0;i<12;i++){
		if(i==2) {
			indata >> Ntot;
				//cout << "Ntot " << Ntot<<endl;
		}
		if(i==3){
			indata >> ntype;
				//cout << "Ntypes: " << ntype<<endl;
		}
		if(i==4){
			indata >> nbonds;
				//cout << "Nbonds: " << nbonds<<endl;
		}
		if(i==5){
			indata >> nbondtypes;
				//cout << "Nbondtypes: " << nbondtypes<<endl;
		}
		if(i==6){
			indata >> nangles;
				//cout << "Nangles: " << nangles<<endl;
		}
		if(i==7){
			indata >> nangletypes;
				//cout << "Nangletypes: " << nangletypes<<endl;
		}
		if(i==9) {
			indata >> Lminx >> Lmaxx;
				//cout << "L " << Lminx<< " " << Lmaxx <<endl;
			Lx = Lmaxx-Lminx;
				//cout << "Lx " << Lx <<endl;
		}
		if(i==10) {
			indata >> Lminy >> Lmaxy;
				//cout << "L " << Lminy << " " << Lmaxy <<endl;
			Ly = Lmaxy-Lminy;
				//cout << "Ly " << Ly <<endl;
		}
		if(i==11) {
			indata >> Lminz >> Lmaxz;
				//cout << "L " << Lminz << " " << Lmaxz <<endl;
			Lz = Lmaxz-Lminz;
				//cout << "Lz " << Lz <<endl;
		}
		else{
			getline(indata,dummy);
				//cout<<"d"<<dummy<<endl;
		}
	}
	
	while(1==1){
		getline(indata,dummy);
		string str=dummy;
		string str2 ("Atoms");
		if (str.find(str2) != string::npos)break;
	}
	
		//READ ATOMS
	int s=0;
	int bindex=0;
	for(int n=0; n<Ntot; n++){
		indata >> bindex>> mol>>type>> x>>y>>z>>ix>>iy>>iz;
			//cout << id <<" " << x<<endl;
		id=mol;
		if(type==1 || type==5){
			PolymerWrap[0][id-1][0]=x;
			PolymerWrap[0][id-1][1]=y;
			PolymerWrap[0][id-1][2]=z;
			PolymerImg[0][id-1][0]=ix;
			PolymerImg[0][id-1][1]=iy;
			PolymerImg[0][id-1][2]=iz;
				//
			Polymer[0][id-1][0]=x+Lx*ix;
			Polymer[0][id-1][1]=y+Ly*iy;
			Polymer[0][id-1][2]=z+Lz*iz;
			Polymer[0][id-1][3]=mol;
			Polymer[0][id-1][4]=type;
			Polymer[0][id-1][5]=bindex;
		}
		if(type==2 || type==6){
			patch1[0][id-1][0]=x;
			patch1[0][id-1][1]=y;
			patch1[0][id-1][2]=z;
			patch1[0][id-1][3]=mol;
			patch1[0][id-1][4]=type;
			patch1[0][id-1][5]=bindex;
			p1Img[0][id-1][0]=ix;
			p1Img[0][id-1][1]=iy;
			p1Img[0][id-1][2]=iz;
		}
		if(type==3 || type==7){
			patch2[0][id-1][0]=x;
			patch2[0][id-1][1]=y;
			patch2[0][id-1][2]=z;
			patch2[0][id-1][3]=mol;
			patch2[0][id-1][4]=type;
			patch2[0][id-1][5]=bindex;
			p2Img[0][id-1][0]=ix;
			p2Img[0][id-1][1]=iy;
			p2Img[0][id-1][2]=iz;
		}
		if(type==4 || type==8){
			patch3[0][id-1][0]=x;
			patch3[0][id-1][1]=y;
			patch3[0][id-1][2]=z;
			patch3[0][id-1][3]=mol;
			patch3[0][id-1][4]=type;
			patch3[0][id-1][5]=bindex;
			p3Img[0][id-1][0]=ix;
			p3Img[0][id-1][1]=iy;
			p3Img[0][id-1][2]=iz;
		}
		
		if(type==5){
			parB[s]=id-1; //1D position of parB
			s++;
		}
	}
	
	nparb=s;
	
		//READ VELOCITIES
	indata >> dummy;
		//cout << "dum " << dummy<<endl;
	for(int n=0; n<Ntot; n++){
		indata >> id >> vx>>vy>>vz;
			//cout << id <<" " << vx<<endl;
		Velocity[0][id-1][0]=vx;
		Velocity[0][id-1][1]=vy;
		Velocity[0][id-1][2]=vz;
	}
	
		//READ BONDS
	int lefcount=0;
	int nbonds0=Nbeads;
	int sb=0;
	int SpecialBonds[nbonds-nbonds0][4];
	int bid,btype,b1,b2;
	bid=0;btype=0;b1=0;b2=0;
	for(int n=0;n<nbonds;n++)for(int p=0;p<3;p++)Bonds[n][p]=0;
	indata >> dummy;
		//cout << "dum " << dummy<<endl;
	for(int nb=0; nb<nbonds; nb++){
		indata >> bid >> btype>>b1>>b2;
			//cout << bid <<" " << b1 << " " <<b2<<endl;
		Bonds[bid-1][0]=b1;
		Bonds[bid-1][1]=b2;
		Bonds[bid-1][2]=btype;
	}
	
		//READ ANGLES
	int aid,atype,a1,a2,a3;
	aid=0;atype=0;a1=0;a2=0;a3=0;
	for(int na=0;na<nangles;na++)for(int pa=0;pa<4;pa++)Angles[na][pa]=0;
	indata >> dummy;
		//cout << "dum " << dummy<<endl;
	for(int na=0; na<nangles; na++){
		indata >> aid >> atype>>a1>>a2>>a3;
			//cout << aid <<" " << a1 << " " <<a2<<endl; cin.get();
		Angles[na][0]=a1;
		Angles[na][1]=a2;
		Angles[na][2]=a3;
		Angles[na][3]=atype;
	}
	
		//****************************//
	/* parB */
		//****************************//
	int newloads=0;
	int transneigh[Nbeads];
	
	if (time==0){
			//load a parB on middle (at parS) w Gaussian stat
		int number = round(distribution_parS(generator_parS));
		parB[s]=number;
		Polymer[0][number][4]=5;  // change color of polymer bead
		patch1[0][number][4]=6;  // change color of patch
		patch2[0][number][4]=7;  // change color of patch
		patch3[0][number][4]=8;  // change color of patch
		newloads++;
		writeLEF << "#time/(1e5 steps)[0.37ms] id_parB 1Dposition[bead]" <<endl;
		writeLEF << time*1.0/100000 << " " << s << " " << parB[s] <<endl;
			//cout << "added parB at " << number <<endl; cin.get();
	}
	nparb+=newloads;
	
	if (time>0){
		
		/*can load w small prob (loadingrate) at parS*/
		double pload=rand()*1.0/RAND_MAX;
		if(pload<loadingrate){
			int number = round(distribution_parS(generator_parS));
			if(Polymer[0][number][4]!=3){
				parB[nparb]=number;
				Polymer[0][number][4]=5;
				patch1[0][number][4]=6;
				patch2[0][number][4]=7;
				patch3[0][number][4]=8;
				
				newloads++;
			}
		}
		nparb+=newloads;
		
		/*can diffuse */
			//loop over active parB molecules
		for(int ns=0;ns<nparb; ns++){
			cout << "##Loop over parB on the polymer = " << ns <<  endl;
			double pmove=rand()*1.0/RAND_MAX;
				//see top of this c++ for calculation of rates
			if(pmove<diffrate){
				cout << "-- wants to move" <<  endl;
				int dir=rand()%2*2-1; //-1 or +1
				if(dir==1 && parB[ns]<Nbeads-1 && Polymer[0][parB[ns]+1][4]!=3){
					Polymer[0][parB[ns]][4]=1;
					patch1[0][parB[ns]][4]=2;
					patch2[0][parB[ns]][4]=3;
					patch3[0][parB[ns]][4]=4;
					parB[ns]++;
					Polymer[0][parB[ns]][4]=5;
					patch1[0][parB[ns]][4]=6;
					patch2[0][parB[ns]][4]=7;
					patch3[0][parB[ns]][4]=8;
				}
				if(dir==-1 && parB[ns]>0 && Polymer[0][parB[ns]-1][4]!=3){
					Polymer[0][parB[ns]][4]=1;
					patch1[0][parB[ns]][4]=2;
					patch2[0][parB[ns]][4]=3;
					patch3[0][parB[ns]][4]=4;
					parB[ns]--;
					Polymer[0][parB[ns]][4]=5;
					patch1[0][parB[ns]][4]=6;
					patch2[0][parB[ns]][4]=7;
					patch3[0][parB[ns]][4]=8;
				}
				cout << "-> has moved "<<  endl;
			}
			if(time%100==0)writeLEF << time*1.0/100000 << " " << ns << " " << parB[ns] <<endl;
			
			
			/*can recruit */
			double prec=rand()*1.0/RAND_MAX;
			if(prec<recruitment){
				cout << "-- wants to recruit" <<  endl;
					//cis or trans?
				double pcis=rand()*1.0/RAND_MAX;
				if(pcis<cisrec){ //cis
					cout << "-- wants to do cis" <<  endl;
					int dir=rand()%2*2-1; //-1 or +1
					if(parB[ns]+dir>0 && parB[ns]+dir < Nbeads-1 && Polymer[0][parB[ns]+dir][4]!=3){
						newloads++;
						Polymer[0][parB[ns]+dir][4]=5;
						patch1[0][parB[ns]+dir][4]=6;
						patch2[0][parB[ns]+dir][4]=7;
						patch3[0][parB[ns]+dir][4]=8;
						parB[nparb-1+newloads]=parB[ns]+dir; //assign new parB
					}
					cout << " -- cis " <<endl;
				}
				else{ //trans
					cout << "-- wants to do trans" <<  endl;
					int nn=0;
					for(int n=0;n<Nbeads;n++){
						if(distance(Polymer[0][parB[ns]],Polymer[0][n])<3.0 && fabs(parB[ns]-n)>2){
							transneigh[nn]=n;
							nn++;
							cout << "--- Found neighbour " << n << " out of " << nn << "neighbours" <<endl;
						}
					}
						//pick one of the nn trans neighbours
					if(nn>0){
						int mytrans=rand()*1.0/RAND_MAX*nn;
						cout << "--- picked " << mytrans <<endl;
						Polymer[0][transneigh[mytrans]][4]=5;
						patch1[0][transneigh[mytrans]][4]=6;
						patch2[0][transneigh[mytrans]][4]=7;
						patch3[0][transneigh[mytrans]][4]=8;
						newloads++;
						parB[nparb-1+newloads]=transneigh[mytrans]; //assign new parB
					}
					cout << " -- trans " <<endl;
				}
				cout << "-> has recruited "<<  endl;
			}//close recruitment
			
			/*can unload */
			double punload=rand()*1.0/RAND_MAX;
			if(punload<unloadingrate){
				cout << "-- wants to unload" <<  endl;
				Polymer[0][parB[ns]][4]=1;
				patch1[0][parB[ns]][4]=2;
				patch2[0][parB[ns]][4]=3;
				patch3[0][parB[ns]][4]=4;
				newloads--;
				cout << "-> has unloaded "<<  endl;
			}
			
		}//close loop over active parB
		
		nparb+=newloads;
		
	} //close if time > 0
	
		//****************************//
	/*  end of parB */
		//****************************//
	
		///////////////////////////////////////////////////////////////////
		///////////////////	WRITE NEW FILE  /////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
	
		//OUTFILE
	stringstream writeFile;
	writeFile <<argv[3]<<argv[1];
	ofstream write(writeFile.str().c_str());
	cout << "writing on .... " <<writeFile.str().c_str()<<endl;
	write.precision(19);
	write << "LAMMPS data file via write_data, version 15 May 2015, timestep = "<< argv[1]<< endl;
	write <<endl;
	write << Ntot  << " atoms "<<endl;
	write << ntype  << " atom types "<<endl;
		//write<< 10 << " extra bond per atom " <<endl;
	write << nbonds << " bonds "<<endl;
	write << nbondtypes << " bond types "<<endl;
	write << nangles << " angles "<<endl;
	write << nangletypes << " angle types "<<endl;
	write << "\n";
	write << Lminx << " " << Lmaxx << " xlo xhi"<<endl;
	write << Lminy << " " << Lmaxy << " ylo yhi"<<endl;
	write << Lminz << " " << Lmaxz << " zlo zhi"<<endl;
	write << "\nMasses \n"<<endl;
	for(int j=0; j<ntype;j++) write << j+1 << " " << 1<< endl;
	
	write << "\nAtoms \n"<<endl;
	for(int n=0; n<Nbeads;n++){
		write << Polymer[0][n][5] << " " << Polymer[0][n][3] << " " <<  Polymer[0][n][4] << " " << Polymer[0][n][0] << " " <<Polymer[0][n][1] << " " << Polymer[0][n][2] << " " << PolymerImg[0][n][0] << " " << PolymerImg[0][n][1] << " " << PolymerImg[0][n][2] << endl;
		write << patch1[0][n][5] << " " << patch1[0][n][3] << " " <<  patch1[0][n][4] << " " << patch1[0][n][0] << " " <<patch1[0][n][1] << " " << patch1[0][n][2] << " " << p1Img[0][n][0] << " " << p1Img[0][n][1] << " " << p1Img[0][n][2] << endl;
		write << patch2[0][n][5] << " " << patch2[0][n][3] << " " <<  patch2[0][n][4] << " " << patch2[0][n][0] << " " <<patch2[0][n][1] << " " << patch2[0][n][2] << " " << p2Img[0][n][0] << " " << p2Img[0][n][1] << " " << p2Img[0][n][2] << endl;
		write << patch3[0][n][5] << " " << patch3[0][n][3] << " " <<  patch3[0][n][4] << " " << patch3[0][n][0] << " " <<patch3[0][n][1] << " " << patch3[0][n][2] << " " << p3Img[0][n][0] << " " << p3Img[0][n][1] << " " << p3Img[0][n][2] << endl;
	}
	
	
	write << "\n\n\nVelocities\n"<<endl;
	for(int n=0; n<Ntot;n++) write << n+1 << " " << Velocity[0][n][0] << " "<< Velocity[0][n][1] << " "<<  Velocity[0][n][2] << endl;
	
	
	write << "\nBonds \n"<<endl;
	for(int n=0; n<nbonds;n++) {
		write << n+1 << " " << Bonds[n][2] << " " << Bonds[n][0] << " " << Bonds[n][1] << endl;
	}
	
	write << "\nAngles \n"<<endl;
	for(int na=0; na<nangles;na++){
		write << na+1 << " " << Angles[na][3] << " " << Angles[na][0] << " " << Angles[na][1] << " " << Angles[na][2]<< endl;
			//if(Angles[na][1]==1){cout<<"Angles AHAHA "<<na+1<<" "<<Angles[na][0]<< " " <<Angles[na][1] << endl; cin.get();cin.get();}
		
	}
	cout << " ###################################### FIN ############################## " <<endl;
	
	return 0;
	
}

double distance(double v1[6], double v2[6]){
	double d=0;
	
	d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);
	
	return sqrt(d);
}

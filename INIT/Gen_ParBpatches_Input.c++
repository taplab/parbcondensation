using namespace std;
#include<iostream>
#include<cmath>
#include<fstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

const int typetot=4;

const int Nmmax=20;
const int Nppolys=10000;

const int Nmaxmax=Nmmax*Nppolys;
double poly[Nppolys][Nmmax][3];
double patch1[Nppolys][Nmmax][3];
double patch2[Nppolys][Nmmax][3];
double patch3[Nppolys][Nmmax][3];
const double L = 50;

int idpoly[Nppolys][Nmmax];
int idp1[Nppolys][Nmmax];
int idp2[Nppolys][Nmmax];
int idp3[Nppolys][Nmmax];

int type[Nppolys][Nmmax];
//int typepatch1[Npoly][Nmax];
int mol[Nppolys][Nmmax];
int molpatch1[Nppolys][Nmmax];
int molpatch2[Nppolys][Nmmax];
int molpatch3[Nppolys][Nmmax];

int bonds[Nmaxmax][2];
int angles1[Nmaxmax][3]; //backbone
int angles2[Nmaxmax][3]; //bead-patch-bead
int dihe1[Nmaxmax][4]; 
int dihe2[Nmaxmax][4];

int Nmax;
int Npolys;

void computeLk(int n, int m);
double gaussLnkNumb(double c1[][3], double c2[][3]);

int main(int argc, char* argv[]){
srand(time(NULL));

cout << "1: outfile; 2: Npolys; 3:Nbeads. "<<endl;
Npolys=atoi(argv[2]); 
Nmax=atoi(argv[3]);
ofstream write;
write.open(argv[1]);
double xm,ym,zm;
double typem,molec,idm;
int check=1;
//redo until all unlinked
while(check==1){
idm=1;
molec=1;

//create centerline + 3 orthogonal patches
for(int n=0; n<Npolys; n++){
double tilt=((double)(rand())/(double)(RAND_MAX))*M_PI/2.;
	for(int m=0; m<Nmax; m++){
		if(m==0){		
		poly[n][m][0]=(((double)(rand())/(double)(RAND_MAX))-0.5)*L/2.;
		poly[n][m][1]=(((double)(rand())/(double)(RAND_MAX))-0.5)*L/2.;
		poly[n][m][2]=(((double)(rand())/(double)(RAND_MAX))-0.5)*L/2.;
		}
		if(m>0){
		//RW
		//double theta=((double)(rand)/(double)(RAND_MAX))*2*M_PI;
		//double phi=((double)(rand)/(double)(RAND_MAX))*M_PI;
		//poly[n][m][0]=poly[n][m-1][0]+0.7*cos(theta)*sin(phi);
		//poly[n][m][1]=poly[n][m-1][0]+0.7*sin(theta)*sin(phi);
		//poly[n][m][2]=poly[n][m-1][0]+0.7*cos(phi);
		//CIRCLE
		double theta=m*1.0/Nmax*2*M_PI;
		poly[n][m][0]=poly[n][m-1][0]+1*cos(theta)*cos(tilt); //tilt in xz plane
		poly[n][m][1]=poly[n][m-1][1]+1*sin(theta);
		poly[n][m][2]=poly[n][m-1][2]+1*cos(theta)*sin(tilt);
		}
	idpoly[n][m]=idm; idm++;
	mol[n][m]=molec;	

	double a=pow(2,1./6.)/2.;
	//tangent 
	double t[3];
	t[0]=poly[n][m][0]-poly[n][m-1][0];
	t[1]=poly[n][m][1]-poly[n][m-1][1];
	t[2]=poly[n][m][2]-poly[n][m-1][2];
	if(m==0){t[0]=cos(tilt);t[1]=0;t[2]=sin(tilt);}
	double normt=sqrt(t[0]*t[0]+t[1]*t[1]+t[2]*t[2]);	
	//patch3 = opposite to patch1
	double no[3];
	no[0]=-sin(tilt); no[1]=0.0; no[2]=cos(tilt);
	no[0]=-sin(tilt); no[1]=0.0; no[2]=cos(tilt);
	patch3[n][m][0]=poly[n][m][0]-a*no[0];
	patch3[n][m][1]=poly[n][m][1]-a*no[1];
	patch3[n][m][2]=poly[n][m][2]-a*no[2];
	idp3[n][m]=idm; idm++;
	molpatch3[n][m]=molec;
	//patch 1 = 'x' orthogonal to ribbon face
	patch1[n][m][0]=poly[n][m][0]+a*no[0];
	patch1[n][m][1]=poly[n][m][1]+a*no[1];
	patch1[n][m][2]=poly[n][m][2]+a*no[2];
	idp1[n][m]=idm; idm++;
	molpatch1[n][m]=molec;
	//patch 2 = 'y' =  radial, found as cross prod p3 x p1
	double b[3]; 
 	b[0]=t[1]*no[2]-no[1]*t[2];
	b[1]=-t[0]*no[2]+t[2]*no[0];
	b[2]=t[0]*no[1]-t[1]*no[0];
	double normb=sqrt(b[0]*b[0]+b[1]*b[1]+b[2]*b[2]);
	patch2[n][m][0]=poly[n][m][0]+b[0]/normb*a;
	patch2[n][m][1]=poly[n][m][1]+b[1]/normb*a;
	patch2[n][m][2]=poly[n][m][2]+b[2]/normb*a;
	idp2[n][m]=idm; idm++;
	molpatch2[n][m]=molec;
	//
	//
	molec++;	
	}
}

check=0;
//check linking number
cout << "RE-TRYING!" <<endl;
int lk=0;
for(int n=0; n<Npolys; n++){
cout<< "computing lk with " << n <<endl;
 for(int nn=n+1; nn<Npolys; nn++){
	lk+=gaussLnkNumb(poly[n],poly[nn]);
	if(lk!=0){
	cout << "linked with " << nn<<" AIAIAIAIAI!"<<endl;
	check=1;
	break;
	}
	if(check==1)break;
	}
}

if(check==0)break;
}

//bonds
//between consecutive beads in center line
int cbonds=0;
for(int n=0;n<Npolys;n++){
	for(int m=0; m<Nmax-1; m++){
		bonds[cbonds][0]=idpoly[n][m];
		bonds[cbonds][1]=idpoly[n][(m+1)%Nmax];		
		cbonds++;
	}
}

//angles
//between triplets of center line
int cangles1=0;
for(int n=0; n<Npolys; n++){
	for(int m=0; m<Nmax-2; m++){
		angles1[cangles1][0]=idpoly[n][m];
		angles1[cangles1][1]=idpoly[n][(m+1)%Nmax];
		angles1[cangles1][2]=idpoly[n][(m+2)%Nmax];
		cangles1++;
	}
}


//between two beads center line + patch3 
/*
int cangles2=0;
for(int n=0; n<Npolys; n++){
	for(int m=0; m<Nmax; m++){
		angles2[cangles2][0]=idpoly[n][m];
		angles2[cangles2][1]=idp3[n][m];
		angles2[cangles2][2]=idpoly[n][(m+1)%Nmax];
		cangles2++;
	}
}

//dihedrals
int cdihe1=0;
for(int n=0; n<Npolys; n++){
	for(int m=0; m<Nmax; m++){
		dihe1[cdihe1][0]=idp1[n][m];
		dihe1[cdihe1][1]=idpoly[n][m];
		dihe1[cdihe1][2]=idpoly[n][(m+1)%Nmax];
		dihe1[cdihe1][3]=idp1[n][(m+1)%Nmax];
		cdihe1++;
	}
}
int cdihe2=0;
for(int n=0; n<Npolys; n++){
	for(int m=0; m<Nmax; m++){
		dihe2[cdihe2][0]=idp2[n][m];
		dihe2[cdihe2][1]=idpoly[n][m];
		dihe2[cdihe2][2]=idpoly[n][(m+1)%Nmax];
		dihe2[cdihe2][3]=idp2[n][(m+1)%Nmax];
		cdihe2++;
	}
}
*/

write << "LAMMPS data file from restart file: timestep = 0, procs = 1"<<endl;
write <<endl;
write << Npolys*Nmax*4 << " atoms"<<endl;
write << cbonds << " bonds"<<endl;
write << cangles1 << " angles"<<endl;
//write << cdihe1+cdihe2 << " dihedrals"<<endl;
write <<endl;
write << typetot << " atom types" <<endl;
write << 1 << " bond types" <<endl;
write << 1 << " angle types" <<endl;
//write << 1 << " dihedral types" <<endl;
write <<endl;
write << -L/2. << " " << L/2. << " xlo xhi"<<endl;
write << -L/2. << " " << L/2. << " ylo yhi"<<endl;
write << -L/2. << " " << L/2. << " zlo zhi"<<endl;
write <<endl;
write << "Masses"<<endl;
write <<endl;
for(int tt=0;tt<typetot; tt++)write << tt+1 << " 1"<<endl;
write <<endl;
write << "Atoms"<<endl;
write <<endl;
for(int n=0;n<Npolys; n++){
	for(int m=0;m<Nmax;m++){
		write << idpoly[n][m] << " "<< mol[n][m] << " " << 1 << " " << poly[n][m][0] << " " << poly[n][m][1] << " " << poly[n][m][2]<<" 0 0 0 "<< endl;
		write << idp1[n][m] << " "<< molpatch1[n][m] << " " << 2 << " " << patch1[n][m][0] << " " << patch1[n][m][1] << " " << patch1[n][m][2]<<" 0 0 0 "<< endl;
		write << idp2[n][m] << " "<< molpatch2[n][m] << " " << 3 << " " << patch2[n][m][0] << " " << patch2[n][m][1] << " " << patch2[n][m][2]<<" 0 0 0 "<< endl;
		write << idp3[n][m] << " "<< molpatch3[n][m] << " " << 4 << " " << patch3[n][m][0] << " " << patch3[n][m][1] << " " << patch3[n][m][2]<<" 0 0 0 "<< endl;
/*
cout << "distances atom " << mol[n][m] << endl;
double d1=sqrt(pow(patch1[n][m][0]-poly[n][m][0],2.0)+pow(patch1[n][m][1]-poly[n][m][1],2.0)+pow(patch1[n][m][2]-poly[n][m][2],2.0)); 
double d2=sqrt(pow(patch2[n][m][0]-poly[n][m][0],2.0)+pow(patch2[n][m][1]-poly[n][m][1],2.0)+pow(patch2[n][m][2]-poly[n][m][2],2.0)); 
double d3=sqrt(pow(patch3[n][m][0]-poly[n][m][0],2.0)+pow(patch3[n][m][1]-poly[n][m][1],2.0)+pow(patch3[n][m][2]-poly[n][m][2],2.0)); 
cout << "d1 " << d1 << " d2 " << d2 << " d3 " << d3<<endl;  
*/
}
}
write <<endl;
write <<endl;
write << "Bonds"<<endl;
write <<endl;
for(int nc=0;nc<cbonds; nc++)write << nc+1 << " 1 " << bonds[nc][0] << " " << bonds[nc][1] <<endl;
write <<endl;
write << "Angles"<<endl;
write <<endl;
for(int na=0;na<cangles1; na++)write << na+1 << " 1 "<< angles1[na][0] << " " << angles1[na][1] << " " << angles1[na][2] << endl;
//for(int na=0;na<cangles2; na++)write << na+1 << " 2 "<< angles2[na][0] << " " << angles2[na][1] << " " << angles2[na][2] << endl;
//write <<endl;
//write << "Dihedrals"<<endl;
//write <<endl;
//for(int da=0;da<cdihe1; da++)write << da+1 << " 1 "<< dihe1[da][0] << " " << dihe1[da][1] << " " << dihe1[da][2] << " "<< dihe1[da][3]<< endl;
//for(int na=0;na<cdihe2; na++)write << cdihe1+na+1 << " 1 "<< dihe2[na][0] << " " << dihe2[na][1] << " " << dihe2[na][2]<< " "<< dihe2[na][3] << endl;
//write<<endl;

return 0; 
}



double gaussLnkNumb(double c1[][3], double c2[][3]){
double lk=0;
long double num=0;
double dr1[3],dr2[3],r[3],cross[3];
long double dist, dist3; 
int n1=Nmax;
int n2=Nmax;
//for(int n =0 ;n<n1; n++)cout << "1 "<<  c1[n][0] << " " << c1[n][1] << " " <<c1[n][2] << endl;
//for(int m=0;m<n2;m++)cout << "2 "<<  c2[m][0] << " " << c2[m][1] << " " <<c2[m][2] << endl;

for(int n =0 ;n<n1; n++){
	for(int m=0;m<n2;m++){
		r[0] = c2[m][0] - c1[n][0];
		r[1] = c2[m][1] - c1[n][1];
		r[2] = c2[m][2] - c1[n][2];
		dist = sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]);
		dist3 = dist*dist*dist;
		
		dr1[0] = c1[(n+1)%n1][0] - c1[n][0];
		dr1[1] = c1[(n+1)%n1][1] - c1[n][1];
		dr1[2] = c1[(n+1)%n1][2] - c1[n][2];
		
		dr2[0] = c2[(m+1)%n2][0] - c2[m][0];
		dr2[1] = c2[(m+1)%n2][1] - c2[m][1];
		dr2[2] = c2[(m+1)%n2][2] - c2[m][2];
		
		cross[0] = dr1[1]*dr2[2] - dr1[2]*dr2[1];
		cross[1] = dr1[2]*dr2[0] - dr1[0]*dr2[2];
		cross[2] = dr1[0]*dr2[1] - dr1[1]*dr2[0];
		
		num = r[0]*cross[0] + r[1]*cross[1] + r[2]*cross[2];
		
		lk += num*1.0/dist3;
		
	//if(ch==1)cout << n << " " << m << " " << dist << " " << num << " " << lk*1.0/(4.0*M_PI)  <<endl;
	}
}	
	
return round(lk*1.0/(4.0*M_PI));
}

